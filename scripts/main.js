/**
 * Main JavaScript
 * Mybabyspa
 *
 * Copyright (c) 2014. by Way2CU, http://way2cu.com
 * Authors:
 */
var Caracal = Caracal || {};

function on_site_load() {
 	// create new page control
 	Caracal.slides = new PageControl('div#slides', 'div.slide');

 	// configure page control
 	Caracal.slides
 				.setInterval(10000)
 				.setWrapAround(true)
 				.setPauseOnHover(true)
 				.attachNextControl($('a.arrow.next'))
 				.attachPreviousControl($('a.arrow.previous'))
 				.attachControls('div#controls a');

 	// create image lightbox
 	Caracal.lightbox = new LightBox('a.image.direct', false, false, true);

 	/**
 	**** making the testimonials
 	**/

 	if ($('div.testimonial').length > 0) {
 		Caracal.testimonial_pages = new PageControl('div.testimonials_container', 'div.testimonial');
 		Caracal.testimonial_pages.attachControls('div.testimonials_container nav#script a');
 	}

 	$('form').on('analytics-event', function(event, data) {
            if (!data.error)
                    dataLayer.push({
                    	'event':'leadSent'
                    });
    });
}

$(on_site_load);
